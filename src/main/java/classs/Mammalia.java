package classs;

import clade.Mammaliaformes;

/**
 * Mammals (from Latin mamma "breast") are vertebrate animals constituting the class Mammalia (/məˈmeɪliə/), and
 * characterized by the presence of mammary glands which in females produce milk for feeding (nursing) their young,
 * a neocortex (a region of the brain), fur or hair, and three middle ear bones.
 */
public abstract class Mammalia extends Mammaliaformes implements IsClasss {

  public boolean hasMammaryGlands() {
    return true;
  }
}
