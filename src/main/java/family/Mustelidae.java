package family;

import misc.HasWikipediaPage;
import misc.Language;
import order.Carnivora;

import java.util.HashMap;
import java.util.Map;

import static misc.Language.ENGLISH;

/**
 * Mustelidae (/ˌmʌˈstɛlɪdi/; from Latin mustela, weasel) are a family of carnivorous mammals, including weasels,
 * badgers, otters, ferrets, martens, minks, and wolverines, among others. Mustelids (/ˈmʌstəlɪd/) are a diverse
 * group and form the largest family in the order Carnivora, suborder Caniformia.
 */
public abstract class Mustelidae extends Carnivora implements IsFamily, HasWikipediaPage {

  @Override
  public Map<Language, String> getWikipediaPages() {
    Map<Language, String> map = new HashMap<>();
    map.put(ENGLISH, "https://en.wikipedia.org/wiki/Mustelidae");
    return map;
  }
}
