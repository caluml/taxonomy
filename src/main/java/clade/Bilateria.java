package clade;

/**
 * The bilateria /ˌbaɪləˈtɪəriə/ or bilaterians are animals with bilateral symmetry as an embryo, i.e. having a left
 * and a right side that are mirror images of each other. This also means they have a head and a tail
 * (anterior-posterior axis) as well as a belly and a back (ventral-dorsal axis).
 */
public abstract class Bilateria extends Parahoxozoa implements IsClade {

}
