package classs;

import misc.HasWikipediaPage;
import misc.Language;
import phylum.Chordata;

import java.util.HashMap;
import java.util.Map;

import static misc.Language.ENGLISH;

public abstract class Reptilia extends Chordata implements IsClasss, HasWikipediaPage {

  @Override
  public Map<Language, String> getWikipediaPages() {
    Map<Language, String> map = new HashMap<>();
    map.put(ENGLISH, "https://en.wikipedia.org/wiki/Reptilia");
    return map;
  }
}
