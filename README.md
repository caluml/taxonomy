This project intends to represent entries in the biological taxonomy system with a Java class.

The principal ranks in modern use are domain, kingdom, phylum (division is sometimes used in botany in place of phylum), class, order, family, genus, and species

I don't know anything about taxonomy, so I am getting all my information from the Infoboxes on Wikipedia pages.

There is a crude Generator class which converts Infobox content into classes. It has many bugs.


To get an idea about this, start with Species: /caluml/taxonomy/-/tree/master/src/main/java/species


If you want to contribute or correct, feel free to submit merge requests.