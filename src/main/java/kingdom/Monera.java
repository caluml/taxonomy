package kingdom;

/**
 * Monera is a kingdom that contains unicellular organisms with a prokaryotic cell organization, such as bacteria.
 * They are single-celled organisms with no true nuclear membrane.
 */
public abstract class Monera implements IsKingdom {

}
