package subgenus;

/**
 * Mus is a subgenus of the rodent genus Mus.
 */
public abstract class Mus extends genus.Mus implements IsSubgenus {

}
