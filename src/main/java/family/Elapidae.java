package family;

import suborder.Serpentes;

/**
 * Elapidae (/ɪˈlæpɪdiː/, commonly known as elapids /ˈɛləpɪdz/; Ancient Greek: ἔλλοψ éllops "sea-fish"[6]) is a family
 * of venomous snakes characterized by hollow, permanently erect, relatively short fangs in the front of the mouth that
 * channel venom into their prey.
 */
public abstract class Elapidae extends Serpentes implements IsFamily {

}
