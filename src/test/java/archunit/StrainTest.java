package archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;
import strain.IsStrain;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static org.assertj.core.api.Assertions.assertThat;

public class StrainTest {

  @Test
  public void Implement_interface() {
    JavaClasses classes = new ClassFileImporter().importPackages("strain");
    assertThat(classes).isNotEmpty();

    classes()
      .that().areNotInterfaces()
      .should().implement(IsStrain.class)
      .check(classes);
  }
}
