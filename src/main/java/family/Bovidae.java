package family;

import order.Artiodactyla;

/**
 * The Bovidae comprise the biological family of cloven-hoofed, ruminant vertebrates that includes bison, African
 * buffalo, water buffalo, antelopes, sheep, goats, muskoxen, and domestic cattle.
 */
public abstract class Bovidae extends Artiodactyla implements IsFamily {

}
