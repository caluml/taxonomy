package superclass;

import phylum.Chordata;

/**
 * Tetrapods (/ˈtɛtrəpɒd/; from Greek: τετρα- "four" and πούς "foot") are four-limbed (with a few exceptions, such as
 * snakes) animals
 */
public abstract class Tetrapoda extends Chordata implements IsSuperclass {

}
