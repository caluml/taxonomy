package species;

import genus.Ursus;
import misc.ConservationStatus;
import misc.HasConservationStatus;

public class UrsusMaritimus extends Ursus implements IsSpecies, HasConservationStatus {

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.VULNERABLE;
  }
}
