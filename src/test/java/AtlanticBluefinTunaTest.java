import kingdom.Animalia;
import order.Scombriformes;
import org.junit.Test;
import species.IsSpecies;
import species.ThunnusThynnus;

import static org.assertj.core.api.Assertions.assertThat;

public class AtlanticBluefinTunaTest {

  @Test
  public void AtlanticBluefinTuna() {
    IsSpecies species = new ThunnusThynnus();

    assertThat(species).isInstanceOf(Animalia.class);

    assertThat(species).isInstanceOf(Scombriformes.class);
  }
}
