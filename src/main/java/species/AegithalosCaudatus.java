package species;

import genus.Aegithalos;
import misc.ConservationStatus;
import misc.HasConservationStatus;

public class AegithalosCaudatus extends Aegithalos implements IsSpecies, HasConservationStatus {

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.LEAST_CONCERN;
  }
}
