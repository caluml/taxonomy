package misc;

public interface HasConservationStatus {

  ConservationStatus getConservationStatus();

}
