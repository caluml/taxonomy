package clade;

import subkingdom.Eumetazoa;
import subkingdom.IsSubkingdom;

/**
 * The Parahoxozoa group was defined based on the presence of several gene (sub)classes (HNF, CUT, PROS, ZF, CERS, K50,
 * S50-PRD), as well as Hox/ParaHox-ANTP from which the name of this clade originated.
 */
public abstract class Parahoxozoa extends Eumetazoa implements IsClade {

}
