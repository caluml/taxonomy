import family.Coronaviridae;
import org.junit.Test;
import strain.SarsCv2;
import unranked.Virus;

import static org.assertj.core.api.Assertions.assertThat;

public class SarsCv2Test {

  @Test
  public void SarsCv2Test() {
    SarsCv2 strain = new SarsCv2();

    assertThat(strain).isInstanceOf(Virus.class);
    assertThat(strain).isInstanceOf(Coronaviridae.class);
  }
}
