package species;

import genus.Rhinoceros;
import misc.ConservationStatus;
import misc.HasConservationStatus;

public class RhinocerosSondaicus extends Rhinoceros implements IsSpecies, HasConservationStatus {

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.CRITICALLY_ENDANGERED;
  }
}
