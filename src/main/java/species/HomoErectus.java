package species;

import genus.Homo;

/**
 * Homo erectus (meaning 'upright man') is an extinct species of archaic human from the Pleistocene, with its earliest
 * occurrence about 2 mya, and its specimens are among the first recognisable members of the genus Homo.
 */
public class HomoErectus extends Homo implements IsSpecies {

}
