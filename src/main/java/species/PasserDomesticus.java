package species;

import genus.Passer;
import misc.ConservationStatus;
import misc.HasConservationStatus;

/**
 * The house sparrow (Passer domesticus) is a bird of the sparrow family Passeridae, found in most parts of the world.
 */
public class PasserDomesticus extends Passer implements IsSpecies, HasConservationStatus {

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.LEAST_CONCERN;
  }
}
