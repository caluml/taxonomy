package species;

import genus.Quercus;
import misc.ConservationStatus;
import misc.HasConservationStatus;
import misc.HasWikipediaPage;
import misc.Language;

import java.util.HashMap;
import java.util.Map;

public class QuercusRobur extends Quercus implements IsSpecies, HasWikipediaPage, HasConservationStatus {

  @Override
  public Map<Language, String> getWikipediaPages() {
    Map<Language, String> map = new HashMap<>();
    map.put(Language.ENGLISH, "https://en.wikipedia.org/wiki/Pedunculate_oak");
    return map;
  }

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.LEAST_CONCERN;
  }
}
