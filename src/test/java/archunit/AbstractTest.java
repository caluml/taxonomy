package archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaModifier;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;
import unranked.Virus;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static org.assertj.core.api.Assertions.assertThat;

public class AbstractTest {

  @Test
  public void Should_be_abstract() {
    JavaClasses classes = new ClassFileImporter()
      .importPackages("classs", "superclass", "clade", "division", "domain", "family", "infraclass", "infraorder",
        "genus", "kingdom", "order", "infrakingdom", "subkingdom",
        "subphylum", "phylum", "superphylum",
        "realm",
        "speciessubgroup", "speciescomplex", "speciesgroup",
        "subclass", "subfamily", "superfamily", "subgenus", "suborder", "superorder", "tribe", "unranked");
    assertThat(classes).isNotEmpty();

    classes()
      .that().areNotInterfaces()
      .should().haveModifier(JavaModifier.ABSTRACT)
      .check(classes);
  }

  @Test
  public void Should_not_be_abstract() {
    JavaClasses classes = new ClassFileImporter()
      .importPackages("species");
    assertThat(classes).isNotEmpty();

    // Viruses have strains, so viruses species should be abstract (?)
    classes()
      .that().areNotInterfaces().and().areNotAssignableTo(Virus.class)
      .should().notHaveModifier(JavaModifier.ABSTRACT)
      .check(classes);
  }
}
