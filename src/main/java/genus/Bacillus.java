package genus;

import family.Bacillaceae;

/**
 * Bacillus (Latin "stick") is a genus of Gram-positive, rod-shaped bacteria, a member of the phylum Firmicutes, with
 * 266 named species.
 */
public abstract class Bacillus extends Bacillaceae implements IsGenus {

}
