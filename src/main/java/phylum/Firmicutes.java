package phylum;

import domain.Bacteria;

/**
 * Firmicutes (Latin: firmus, strong, and cutis, skin, referring to the cell wall) are a phylum of bacteria, most of
 * which have gram-positive cell wall structure.
 */
public abstract class Firmicutes extends Bacteria implements IsPhylum {

}
