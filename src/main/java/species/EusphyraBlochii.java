package species;

import genus.Eusphyra;
import misc.ConservationStatus;
import misc.HasConservationStatus;
import misc.HasName;

public class EusphyraBlochii extends Eusphyra implements IsSpecies, HasName, HasConservationStatus {

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.ENDANGERED;
  }

  @Override
  public String getName() {
    return "Winghead shark";
  }
}
