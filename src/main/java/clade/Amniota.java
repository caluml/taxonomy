package clade;

/**
 * Amniotes are tetrapods (descendants of four-limbed and backboned animals) that are characterised by having an egg
 * equipped with an amnion, an adaptation to lay eggs on land rather than in water as the anamniotes (including frogs)
 * typically do.
 */
public abstract class Amniota extends Reptiliomorpha implements IsClade {

}
