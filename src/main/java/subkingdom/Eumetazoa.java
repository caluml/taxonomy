package subkingdom;

import kingdom.Animalia;

/**
 * Characteristics of eumetazoans include true tissues organized into germ layers, the presence of neurons, and an
 * embryo that goes through a gastrula stage.
 */
public abstract class Eumetazoa extends Animalia implements IsSubkingdom {

}
