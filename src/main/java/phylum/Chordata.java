package phylum;

import superphylum.Deuterostomia;

/**
 * A chordate (/ˈkɔːrdeɪt/) is an animal of the phylum Chordata. During some period of their life cycle, chordates
 * possess a notochord, a dorsal nerve cord, pharyngeal slits, and a post-anal tail: these four anatomical features
 * define this phylum. Chordates are also bilaterally symmetric, and have a coelom, metameric segmentation, and
 * circulatory system.
 */
public abstract class Chordata extends Deuterostomia implements IsPhylum {

  public boolean hasPostAnalTail() {
    return true;
  }
}
