package misc;

public interface HasName {

  String getName();

}
