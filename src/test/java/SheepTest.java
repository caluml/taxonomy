import classs.Mammalia;
import classs.Reptilia;
import kingdom.Animalia;
import org.junit.Test;
import species.OvisAries;

import static org.assertj.core.api.Assertions.assertThat;

public class SheepTest {

  @Test
  public void Sheep() {
    OvisAries species = new OvisAries();

    assertThat(species).isInstanceOf(Animalia.class);
    assertThat(species).isInstanceOf(Mammalia.class);
    assertThat(species).isNotInstanceOf(Reptilia.class);

    assertThat(species.hasMammaryGlands()).isTrue();
  }
}
