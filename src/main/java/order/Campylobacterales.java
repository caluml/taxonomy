package order;

import classs.Epsilonproteobacteria;

/**
 * The Campylobacterales are an order of Proteobacteria which make up the epsilon subdivision, together with the small
 * family Nautiliaceae. Like all Proteobacteria, they are Gram-negative. Most of the species are microaerophilic.
 */
public abstract class Campylobacterales extends Epsilonproteobacteria implements IsOrder {

}
