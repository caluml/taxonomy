package species;

import genus.Xenopsylla;
import misc.HasWikipediaPage;
import misc.Language;

import java.util.HashMap;
import java.util.Map;

public class XenopsyllaCheopis extends Xenopsylla implements IsSpecies, HasWikipediaPage {

  @Override
  public Map<Language, String> getWikipediaPages() {
    Map<Language, String> map = new HashMap<>();
    map.put(Language.ENGLISH, "https://en.wikipedia.org/wiki/Xenopsylla_cheopis");
    return map;
  }
}
