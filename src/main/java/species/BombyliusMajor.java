package species;

import genus.Bombylius;

/**
 * Bombylius major (commonly named the large bee-fly or the dark-edged bee-fly) is a parasitic bee mimic fly.
 * B. major is the most common type of fly within the Bombylius genus. The fly derives its name from its close
 * resemblance to bumblebees and are often mistaken for them.
 */
public class BombyliusMajor extends Bombylius implements IsSpecies {

}
