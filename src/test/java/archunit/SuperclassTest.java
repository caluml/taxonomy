package archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;
import superclass.IsSuperclass;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static org.assertj.core.api.Assertions.assertThat;

public class SuperclassTest {

  @Test
  public void Implement_interface() {
    JavaClasses classes = new ClassFileImporter().importPackages("superclass");
    assertThat(classes).isNotEmpty();

    classes()
      .that().areNotInterfaces()
      .should().implement(IsSuperclass.class)
      .check(classes);
  }
}
