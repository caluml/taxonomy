package phylum;

import domain.Bacteria;

/**
 * Proteobacteria is a major phylum of Gram-negative bacteria. They include a wide variety of pathogenic genera, such
 * as Escherichia, Salmonella, Vibrio, Helicobacter, Yersinia, Legionellales, and many others. Others are free-living
 * (nonparasitic) and include many of the bacteria responsible for nitrogen fixation.
 */
public abstract class Proteobacteria extends Bacteria implements IsPhylum {

}
