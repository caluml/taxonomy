package species;

import genus.Meles;
import misc.HasName;

public class MelesMeles extends Meles implements IsSpecies, HasName {

  @Override
  public String getName() {
    return "European Badger";
  }
}
