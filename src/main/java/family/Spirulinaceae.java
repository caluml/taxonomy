package family;

import order.Spirulinales;

/**
 * Spirulinaceae is a family of cyanobacteria, the only family in the order Spirulinales.
 * Its members are notable for having coiled trichomes.
 */
public abstract class Spirulinaceae extends Spirulinales implements IsFamily {

}
