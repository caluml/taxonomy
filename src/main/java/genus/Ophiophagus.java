package genus;

import misc.HasWikipediaPage;
import misc.Language;
import subfamily.Elapinae;

import java.util.HashMap;
import java.util.Map;

import static misc.Language.ENGLISH;

public abstract class Ophiophagus extends Elapinae implements IsGenus, HasWikipediaPage {

  @Override
  public Map<Language, String> getWikipediaPages() {
    Map<Language, String> map = new HashMap<>();
    map.put(ENGLISH, "https://en.wikipedia.org/wiki/Ophiophagus");
    return map;
  }
}
