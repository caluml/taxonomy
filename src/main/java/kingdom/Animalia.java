package kingdom;

/**
 * Animals (also called Metazoa) are multicellular eukaryotic organisms that form the biological kingdom Animalia.
 * With few exceptions, animals consume organic material, breathe oxygen, are able to move, can reproduce sexually,
 * and grow from a hollow sphere of cells, the blastula, during embryonic development.
 */
public abstract class Animalia implements IsKingdom {

}
