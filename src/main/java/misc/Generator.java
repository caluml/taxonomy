package misc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Generator {

  public static void main(String[] args) throws IOException {
    Scanner in = new Scanner(System.in);
    String content = "";
    String line;
    while (in.hasNextLine()) {
      line = in.nextLine();
      if (line.equals("")) break;
      content = content + line + "\n";
    }

    List<NewClass> newClasses = extract(content);

    for (NewClass newClass : newClasses) {
      write(newClass);
    }
  }

  public static List<NewClass> extract(String content) {
    List<Map.Entry<String, String>> list = new ArrayList<>();
    String[] lines = content.split("\n");
    for (String row : lines) {
      if (row.contains("\t")) {
        list.add(new AbstractMap.SimpleImmutableEntry<>(row.split(": \t")[0].trim(), row.split("\t")[1].trim()));
      }
    }

    String lastLine = lines[lines.length - 1];
    while (lastLine.contains(" ")) {
      int space = lastLine.indexOf(" ");
      lastLine = lastLine.substring(0, space) + Character.toUpperCase(lastLine.charAt(space + 1)) + lastLine.substring(space + 2);
    }
    list.add(new AbstractMap.SimpleImmutableEntry<>("Species", lastLine));


    List<NewClass> newClasses = new ArrayList<>();

    String previousClassname = null;
    String previousRank = null;
    for (Map.Entry<String, String> entry : list) {
      NewClass newClass = new NewClass();
      String rank = entry.getKey();
      String className = entry.getValue();
      if (className.contains(" ")) continue;
      if (className.startsWith("†")) {
        newClass.setIsExtinct(true);
        className = className.substring(1);
      }
      if (className.endsWith("[1]") || className.endsWith("[2]") || className.endsWith("[3]")) {
        className = className.substring(0, className.length() - 3);
      }
      if (rank.equals("Class")) {
        rank = "Classs";
      }
      File classFile = new File("src/main/java/" + rank.toLowerCase() + "/" + className + ".java");
      if (classFile.exists()) {
        System.err.println(rank + " already exists");
        previousClassname = className;
        previousRank = rank;
        continue;
      }
      newClass.setRank(rank.toLowerCase());
      newClass.setPackage(rank.toLowerCase());
      if (previousClassname != null) {
        newClass.setImport(previousRank.toLowerCase() + "." + previousClassname);
      }
      if (!rank.equalsIgnoreCase("Species")) {
        newClass.setIsAbstract(true);
      }
      newClass.setClass(className);
      if (previousClassname != null) {
        newClass.setExtends(previousClassname);
      }
      if (newClass.getIsExtinct()) {
        newClass.setImplements("Is" + rank + ", IsExtinct");
      } else {
        newClass.setImplements("Is" + rank);
      }
      previousClassname = className;
      previousRank = rank;
      newClasses.add(newClass);
    }
    return newClasses;
  }

  private static void write(NewClass newClass) throws IOException {
    File classFile = new File("src/main/java/" + newClass.getRank() + "/" + newClass.getClasss() + ".java");

    FileWriter fileWriter = new FileWriter(classFile);
    fileWriter.write("package " + newClass.getPackage() + ";\n\n");
    if (newClass.getImport() != null) {
      fileWriter.write("import " + newClass.getImport() + ";\n\n");
    }
    fileWriter.write("public");
    if (newClass.getIsAbstract()) {
      fileWriter.write(" abstract");
    }
    fileWriter.write(" class " + newClass.getClasss());
    if (newClass.getExtends() != null) {
      fileWriter.write(" extends " + newClass.getExtends());
    }
    fileWriter.write(" implements " + newClass.getImplements() + " {\n");
    fileWriter.write("\n}\n");
    fileWriter.flush();
    fileWriter.close();
    System.out.println("Wrote " + classFile.getAbsolutePath());
  }

  public static class NewClass {

    private String aPackage;
    private String anImport;
    private boolean isAbstract;
    private String aClass;
    private String anExtends;
    private String anImplements;
    private String rank;
    private boolean isExtinct;

    public void setPackage(String aPackage) {
      this.aPackage = aPackage;
    }

    public String getPackage() {
      return aPackage;
    }

    public void setImport(String anImport) {
      this.anImport = anImport;
    }

    public String getImport() {
      return anImport;
    }

    public void setIsAbstract(boolean isAbstract) {
      this.isAbstract = isAbstract;
    }

    public boolean getIsAbstract() {
      return isAbstract;
    }

    public void setClass(String aClass) {
      this.aClass = aClass;
    }

    public String getClasss() {
      return aClass;
    }

    public void setExtends(String anExtends) {
      this.anExtends = anExtends;
    }

    public String getExtends() {
      return anExtends;
    }

    public void setImplements(String anInterface) {
      this.anImplements = anInterface;
    }

    public String getImplements() {
      return anImplements;
    }

    public void setRank(String rank) {
      this.rank = rank;
    }

    public String getRank() {
      return rank;
    }

    public void setIsExtinct(boolean isExtinct) {
      this.isExtinct = isExtinct;
    }

    public boolean getIsExtinct() {
      return isExtinct;
    }

    @Override
    public String toString() {
      return "NewClass{" +
        "aPackage='" + aPackage + '\'' +
        ", anImport='" + anImport + '\'' +
        ", isAbstract=" + isAbstract +
        ", aClass='" + aClass + '\'' +
        ", anExtends='" + anExtends + '\'' +
        ", anImplements='" + anImplements + '\'' +
        ", rank='" + rank + '\'' +
        ", isExtinct=" + isExtinct +
        '}';
    }
  }
}
