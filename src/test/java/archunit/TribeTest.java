package archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;
import tribe.IsTribe;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static org.assertj.core.api.Assertions.assertThat;

public class TribeTest {

  @Test
  public void Implement_interface() {
    JavaClasses classes = new ClassFileImporter().importPackages("tribe");
    assertThat(classes).isNotEmpty();

    classes()
      .that().areNotInterfaces()
      .should().implement(IsTribe.class)
      .check(classes);
  }
}
