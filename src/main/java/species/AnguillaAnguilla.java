package species;

import genus.Anguilla;
import misc.ConservationStatus;
import misc.HasConservationStatus;

public class AnguillaAnguilla extends Anguilla implements IsSpecies, HasConservationStatus {

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.CRITICALLY_ENDANGERED;
  }
}
