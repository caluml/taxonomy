package archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import org.junit.Test;
import species.IsSpecies;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static org.assertj.core.api.Assertions.assertThat;

public class SpeciesTest {

  @Test
  public void Implement_interface() {
    JavaClasses classes = new ClassFileImporter().importPackages("species");
    assertThat(classes).isNotEmpty();

    classes()
      .that().areNotInterfaces().and().resideInAPackage("species")
      .should().implement(IsSpecies.class)
      .check(classes);
  }
}
