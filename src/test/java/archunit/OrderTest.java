package archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import order.IsOrder;
import org.junit.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static org.assertj.core.api.Assertions.assertThat;

public class OrderTest {

  @Test
  public void Implement_interface() {
    JavaClasses classes = new ClassFileImporter().importPackages("order");
    assertThat(classes).isNotEmpty();

    classes()
      .that().areNotInterfaces()
      .should().implement(IsOrder.class)
      .check(classes);
  }
}
