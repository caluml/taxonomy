package phylum;

import domain.Bacteria;

/**
 * Cyanobacteria /saɪˌænoʊbækˈtɪəriə/, also known as Cyanophyta, are a phylum consisting of both free-living
 * photosynthetic bacteria and the endosymbiotic plastids that are present in the Archaeplastida autotrophic eukaryotes.
 */
public abstract class Cyanobacteria extends Bacteria implements IsPhylum {

}
