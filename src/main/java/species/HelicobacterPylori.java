package species;

import genus.Helicobacter;

/**
 * Helicobacter pylori, previously known as Campylobacter pylori, is a gram-negative, helically-shaped, microaerophilic
 * bacterium usually found in the stomach. Its helical shape (from which the genus name, helicobacter, derives) is
 * thought to have evolved in order to penetrate the mucoid lining of the stomach and thereby establish infection.
 */
public class HelicobacterPylori extends Helicobacter implements IsSpecies {

}
