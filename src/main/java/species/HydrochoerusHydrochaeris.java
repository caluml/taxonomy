package species;

import genus.Hydrochoerus;
import misc.ConservationStatus;
import misc.HasConservationStatus;

/**
 * The capybara (Hydrochoerus hydrochaeris) is a giant cavy rodent native to South America. It is the largest living
 * rodent in the world.
 */
public class HydrochoerusHydrochaeris extends Hydrochoerus implements IsSpecies, HasConservationStatus {

  @Override
  public ConservationStatus getConservationStatus() {
    return ConservationStatus.LEAST_CONCERN;
  }
}
