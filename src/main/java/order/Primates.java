package order;

import classs.Mammalia;

/**
 * A primate (/ˈpraɪmeɪt/ (from Latin primat-, from primus: "prime, first rank") is a eutherian mammal constituting the
 * taxonomic order Primates. Primates arose 85–55 million years ago first from small terrestrial mammals, which adapted
 * to living in the trees of tropical forests: many primate characteristics represent adaptations to life in this
 * challenging environment, including large brains, visual acuity, color vision, altered shoulder girdle, and dextrous
 * hands.
 */
public abstract class Primates extends Mammalia implements IsOrder {

}
