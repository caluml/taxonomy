package clade;

import superclass.Tetrapoda;

/**
 * Reptiliomorpha have
 * <p>
 * narrow premaxillae (less than half the skull width)
 * vomers taper forward
 * phalangeal formulae (number of joints in each toe) of foot 2.3.4.5.4–5
 */
public abstract class Reptiliomorpha extends Tetrapoda implements IsClade {

}
