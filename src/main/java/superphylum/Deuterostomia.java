package superphylum;

import clade.Nephrozoa;

/**
 * In {@link Deuterostomia}, during the embryo development stage, the anus forms before the mouth.
 */
public abstract class Deuterostomia extends Nephrozoa implements IsSuperphylum {

  public boolean anusFormsBeforeMouth() {
    return true;
  }
}
