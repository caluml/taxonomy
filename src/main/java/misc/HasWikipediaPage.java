package misc;

import java.util.Map;

public interface HasWikipediaPage {

  Map<Language, String> getWikipediaPages();
}
