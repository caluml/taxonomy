import classs.Reptilia;
import kingdom.Animalia;
import org.junit.Test;
import species.OphiophagusHannah;
import species.IsSpecies;
import suborder.Serpentes;

import static org.assertj.core.api.Assertions.assertThat;

public class KingCobraTest {

  @Test
  public void KingCobra() {
    IsSpecies species = new OphiophagusHannah();

    assertThat(species).isInstanceOf(Animalia.class);
    assertThat(species).isInstanceOf(Reptilia.class);

    assertThat(species).isInstanceOf(Serpentes.class);
  }
}
