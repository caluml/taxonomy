import misc.Generator;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class GeneratorTest {

  @Test
  public void Can_parse_Wiki_Infobox_content() {
    String content = getResource("/wikiInfoboxContent");

    List<Generator.NewClass> newClasses = Generator.extract(content);

    assertThat(newClasses.get(0).getPackage()).isEqualTo("kingdom");
    assertThat(newClasses.get(0).getImport()).isNull();
    assertThat(newClasses.get(0).getIsAbstract()).isTrue();
    assertThat(newClasses.get(0).getClasss()).isEqualTo("Somekingdom");
    assertThat(newClasses.get(0).getExtends()).isNull();
    assertThat(newClasses.get(0).getImplements()).isEqualTo("IsKingdom");

    assertThat(newClasses.get(1).getPackage()).isEqualTo("phylum");
    assertThat(newClasses.get(1).getImport()).isEqualTo("kingdom.Somekingdom");
    assertThat(newClasses.get(1).getIsAbstract()).isTrue();
    assertThat(newClasses.get(1).getClasss()).isEqualTo("Somephylum");
    assertThat(newClasses.get(1).getExtends()).isEqualTo("Somekingdom");
    assertThat(newClasses.get(1).getImplements()).isEqualTo("IsPhylum");

    assertThat(newClasses.get(2).getPackage()).isEqualTo("classs");
    assertThat(newClasses.get(2).getImport()).isEqualTo("phylum.Somephylum");
    assertThat(newClasses.get(2).getIsAbstract()).isTrue();
    assertThat(newClasses.get(2).getClasss()).isEqualTo("Someclass");
    assertThat(newClasses.get(2).getExtends()).isEqualTo("Somephylum");
    assertThat(newClasses.get(2).getImplements()).isEqualTo("IsClasss");

    assertThat(newClasses.get(3).getPackage()).isEqualTo("order");
    assertThat(newClasses.get(3).getImport()).isEqualTo("classs.Someclass");
    assertThat(newClasses.get(3).getIsAbstract()).isTrue();
    assertThat(newClasses.get(3).getClasss()).isEqualTo("Someorder");
    assertThat(newClasses.get(3).getExtends()).isEqualTo("Someclass");
    assertThat(newClasses.get(3).getImplements()).isEqualTo("IsOrder");

    assertThat(newClasses.get(4).getPackage()).isEqualTo("suborder");
    assertThat(newClasses.get(4).getImport()).isEqualTo("order.Someorder");
    assertThat(newClasses.get(4).getIsAbstract()).isTrue();
    assertThat(newClasses.get(4).getClasss()).isEqualTo("Somesuborder");
    assertThat(newClasses.get(4).getExtends()).isEqualTo("Someorder");
    assertThat(newClasses.get(4).getImplements()).isEqualTo("IsSuborder");

    assertThat(newClasses.get(5).getPackage()).isEqualTo("infraorder");
    assertThat(newClasses.get(5).getImport()).isEqualTo("suborder.Somesuborder");
    assertThat(newClasses.get(5).getIsAbstract()).isTrue();
    assertThat(newClasses.get(5).getClasss()).isEqualTo("Someinfraorder");
    assertThat(newClasses.get(5).getExtends()).isEqualTo("Somesuborder");
    assertThat(newClasses.get(5).getImplements()).isEqualTo("IsInfraorder");

    assertThat(newClasses.get(6).getPackage()).isEqualTo("family");
    assertThat(newClasses.get(6).getImport()).isEqualTo("infraorder.Someinfraorder");
    assertThat(newClasses.get(6).getIsAbstract()).isTrue();
    assertThat(newClasses.get(6).getClasss()).isEqualTo("Somefamily");
    assertThat(newClasses.get(6).getExtends()).isEqualTo("Someinfraorder");
    assertThat(newClasses.get(6).getImplements()).isEqualTo("IsFamily");

    assertThat(newClasses.get(7).getPackage()).isEqualTo("subfamily");
    assertThat(newClasses.get(7).getImport()).isEqualTo("family.Somefamily");
    assertThat(newClasses.get(7).getIsAbstract()).isTrue();
    assertThat(newClasses.get(7).getClasss()).isEqualTo("Somesubfamily");
    assertThat(newClasses.get(7).getExtends()).isEqualTo("Somefamily");
    assertThat(newClasses.get(7).getImplements()).isEqualTo("IsSubfamily");

    assertThat(newClasses.get(10).getPackage()).isEqualTo("species");
    assertThat(newClasses.get(10).getImport()).isEqualTo("genus.Somegenus");
    assertThat(newClasses.get(10).getIsAbstract()).isFalse();
    assertThat(newClasses.get(10).getClasss()).isEqualTo("SomeSomespecies");
    assertThat(newClasses.get(10).getExtends()).isEqualTo("Somegenus");
    assertThat(newClasses.get(10).getImplements()).isEqualTo("IsSpecies, IsExtinct");

  }

  private String getResource(String path) {
    try (InputStreamReader inputStreamReader = new InputStreamReader(getClass().getResourceAsStream(path));
         BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
      return bufferedReader.lines().collect(Collectors.joining("\n"));
    } catch (IOException e) {
      throw new RuntimeException("Couldn't read " + path, e);
    }
  }
}
