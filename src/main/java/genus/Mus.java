package genus;

import family.Muridae;

/**
 * The genus Mus refers to a specific genus of muroid rodents, all typically called mice (the adjective "muroid" comes
 * from the word "Muroidea", which is a large superfamily of rodents, including mice, rats, voles, hamsters, gerbils,
 * and many other relatives). However, the term mouse can also be applied to species outside of this genus.
 */
public abstract class Mus extends Muridae implements IsGenus {

}
